<?php
/* Requests */
Route::get('/admin/requests/list', 'Admin\RequestsController@list')->name('admin_list_requests');
Route::get('/admin/requests/create', 'Admin\RequestsController@create')->name('admin_create_requests');
Route::get('/admin/requests/{id}/edit', 'Admin\RequestsController@edit')->name('admin_edit_requests');
Route::get('/admin/requests/{id}/delete', 'Admin\RequestsController@delete')->name('admin_delete_requests');
Route::post('/admin/requests/store', 'Admin\RequestsController@store')->name('admin_store_requests');
Route::post('/admin/requests/update', 'Admin\RequestsController@update')->name('admin_update_requests');

/* Services */
Route::get('/admin/services/list', 'Admin\ServicesController@list')->name('admin_list_services');
Route::get('/admin/services/create', 'Admin\ServicesController@create')->name('admin_create_services');
Route::get('/admin/services/{id}/edit', 'Admin\ServicesController@edit')->name('admin_edit_services');
Route::get('/admin/services/{id}/delete', 'Admin\ServicesController@delete')->name('admin_delete_services');
Route::post('/admin/services/store', 'Admin\ServicesController@store')->name('admin_store_services');
Route::post('/admin/services/update', 'Admin\ServicesController@update')->name('admin_update_services');

/* Users */
Route::get('/admin/users/list', 'Admin\UsersController@list')->name('admin_list_users');
Route::get('/admin/users/create', 'Admin\UsersController@create')->name('admin_create_users');
Route::get('/admin/users/{id}/edit', 'Admin\UsersController@edit')->name('admin_edit_users');
Route::get('/admin/users/{id}/delete', 'Admin\UsersController@delete')->name('admin_delete_users');
Route::post('/admin/users/store', 'Admin\UsersController@store')->name('admin_store_users');
Route::post('/admin/users/update', 'Admin\UsersController@update')->name('admin_update_users');

Route::redirect('/admin', '/admin/requests/list');