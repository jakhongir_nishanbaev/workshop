<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\RequestsController@index')->name('main_page');
Route::get('/requests/create', 'Frontend\RequestsController@create')->name('frontend_add_request');
Route::get('/requests/{id}/single', 'Frontend\RequestsController@single')->name('frontend_single_request');
Route::post('/requests/store', 'Frontend\RequestsController@store')->name('frontend_store_request');

Route::get('/home', 'HomeController@index')->name('home');
