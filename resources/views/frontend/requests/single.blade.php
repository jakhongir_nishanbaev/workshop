@extends('layouts.frontend.app')

@section('content')

    <div class="row">
        <div class="col-md-3 pull-left">
            <a href="{{ route('main_page') }}">
                <button class="btn btn-success"> Назад </button>
            </a>
        </div>
    </div>


    <table class="table table-striped">
        {{--<thead>--}}
        {{--<tr>--}}
            {{--<th scope="col">#</th>--}}
            {{--<th scope="col">Название услуги</th>--}}
            {{--<th scope="col">Дата визита</th>--}}
            {{--<th scope="col">Комментарий</th>--}}
        {{--</tr>--}}
        {{--</thead>--}}
        <tbody>

            <tr>
                <td><b>Тип услуги</b></td>
                <td>{{ $request->service->name }}</td>
            </tr>

            <tr>
                <td><b>Дата визита</b></td>
                <td>{{ $request->visit_date }}</td>
            </tr>

            <tr>
                <td><b>Комментарий</b></td>
                <td>{{ $request->comment }}</td>
            </tr>

            <tr>
                <td><b>Дата создания</b></td>
                <td>{{ $request->created_at }}</td>
            </tr>

        </tbody>
    </table>
@endsection('content')