@extends('layouts.frontend.app')

@section('content')

    <div class="row">
        <div class="col-md-3 pull-left">
           <a href="{{ route('frontend_add_request') }}">
               <button class="btn btn-success"> Добавить заявку </button>
           </a>
        </div>
    </div>


    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название услуги</th>
            <th scope="col">Дата визита</th>
            <th scope="col">Комментарий</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $index => $request)
            <tr>
                <th scope="row">{{ ++$index }}</th>
                <td>{{ $request->service->name }}</td>
                <td>{{ $request->visit_date }}</td>
                <td>{{ $request->comment }}</td>
                <td><a href="{{ route('frontend_single_request', ['id' => $request->id]) }}"> <span class="glyphicon glyphicon-eye-open "></span> </a></td>
            </tr>
        @endforeach

        </tbody>
    </table>
@endsection('content')