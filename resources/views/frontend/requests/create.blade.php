@extends('layouts.frontend.app')

@section('content')
    <form method="post" action="{{ route('frontend_store_request') }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="exampleFormControlInput1">Дата визита</label>
            <input name="visit_date" type="date" class="form-control" id="exampleFormControlInput1">
        </div>

        <div class="form-group">
            <label for="exampleFormControlSelect1">Тип услуги</label>
            <select name="service_id" class="form-control" id="exampleFormControlSelect1">
                @foreach($services as $service)
                    <option value="{{$service->id}}">{{ $service->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="exampleFormControlTextarea1">Комментарий</label>
            <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-success" id="exampleFormControlTextarea1" rows="3" value="Создать заявку">
        </div>
    </form>
@endsection('content')