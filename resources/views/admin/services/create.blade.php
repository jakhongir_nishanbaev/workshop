@extends('layouts.admin.app')

@section('content')
    <div class="page-wrapper">

    @include('admin.partials.breadcrumbs')

    <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form class="form-horizontal col-md-6" method="post" action="{{ route("admin_store_services") }}">
                            {{ csrf_field() }}

                            <div class="card-body">
                                <h4 class="card-title">Добавить услугу</h4>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Название</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="name" class="form-control" id="lname" placeholder="Название услуги...">
                                    </div>
                                </div>



                                {{--<div class="form-group row">--}}
                                    {{--<label for="lname" class="col-sm-3 text-right control-label col-form-label">Поликлиника</label>--}}

                                    {{--<div class="col-sm-9">--}}
                                        {{--<select name="polyclinic_id">--}}
                                            {{--@foreach($polyclinics as $polyclinic)--}}
                                                {{--<option value="{{ $polyclinic->id }}"> {{ $polyclinic->name }} </option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success">Добавить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->

        @include('admin.partials.footer')
    </div>
@endsection