@extends('layouts.admin.app')

@section('content')
    <div class="page-wrapper">

    @include('admin.partials.breadcrumbs')

    <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form class="form-horizontal col-md-6" method="post" action="{{ route("admin_update_requests") }}">
                            {{ csrf_field() }}

                            <div class="card-body">
                                <h4 class="card-title">Редактировать заявку</h4>

                                <input type="hidden" name="id" value="{{ $editItem->id }}">

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Услуга</label>

                                    <div class="col-sm-9">
                                        <select name="service_id" required>
                                            @foreach($services as $service)
                                                <option @if($editItem->service_id == $service->id) selected @endif value="{{ $service->id }}"> {{ $service->name }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Клиент</label>

                                    <div class="col-sm-9">
                                        <select name="user_id" required>
                                            @foreach($users as $user)
                                                <option @if($editItem->user_id == $user->id) selected @endif value="{{ $user->id }}"> {{ $user->login }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Статус заявки</label>

                                    <div class="col-sm-9">
                                        <select name="status" required>
                                            @foreach(\App\Constants\RequestStatuses::statuses() as $index => $status)
                                                <option @if($editItem->status == $index) selected @endif value="{{ $index }}"> {{ $status }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Дата визита</label>

                                    <div class="col-sm-9">
                                        <input type="date" name="visit_date" class="form-control" id="lname" value="{{ date('Y-m-d', strtotime($editItem->visit_date)) }}" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Коментарий</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="comment" class="form-control" id="lname" value="{{ $editItem->comment }}">
                                    </div>
                                </div>

                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->

        @include('admin.partials.footer')
    </div>
@endsection