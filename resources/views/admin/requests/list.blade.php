@extends('layouts.admin.app')

@section('content')
    <div class="page-wrapper">

    @include('admin.partials.breadcrumbs')

        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">

            <div class="row">
                <div class="card col-md-12">
                    <div class="col-md-3">
                        <div class="card-body">
                            <a href="{{ route("admin_create_requests") }}">
                                <button class="btn btn-success">Добавить</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ============================================================== -->
            <!-- Resource items from database -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="card col-md-12">
                    <div class="card-body">
                        <h5 class="card-title">Все заявки</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th> Клиент </th>
                                    <th> Услуга </th>
                                    <th> Дата </th>
                                    <th> Комментарий </th>
                                    <th> Статус заявки </th>

                                    <th> Действия </th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($items as $item)
                                    <tr>
                                        <td> {{ $item->user->login }} </td>
                                        <td> {{ $item->service->name }} </td>
                                        <td> {{ $item->visit_date }} </td>
                                        <td> {{ $item->comment }} </td>
                                        <td> {{ \App\Constants\RequestStatuses::getStatusName($item->status) }} </td>


                                        <td>
                                            <a href="{{ route("admin_edit_requests", ['id' => $item->id]) }}">
                                                <button class="btn btn-info">
                                                    Редактировать
                                                </button>
                                            </a>

                                            <a onclick="return confirm('Уверены, что хотите удалить?');"
                                               href="{{ route("admin_delete_requests", ['id' => $item->id]) }}">
                                                <button class="btn btn-danger">
                                                    Удалить
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Resource items from database -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->

        @include('admin.partials.footer')
    </div>
@endsection