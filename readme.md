
## How to deploy project

Steps
- git clone & composer install
- config .env file - database, project name
- set up virtual hosts (<domain.name>)
- php artisan migrate
- php artisan db:seed
- to create user go to admin panel - http://<domain.name>/admin

## Routes

Links
- http://<domain.name>/ - home page
- http://<domain.name>/admin - admin panel


## Api documentation

- Create link - POST: http://workshop.local/api/create-request 
   #params:
   <b>login</b> - text <br>
   <b>password</b> - text <br>
   <b>visit_date</b> - text <br>
   <b>service_id</b> - int <br>
   <b>comment</b> - text
   
   
- List user's requests - POST: http://workshop.local/api/list-requests
    #params:
    <b>login</b> - text <br>
    <b>password</b> - text <br>