<?php

namespace App\Services\Admin;


use App\Repositories\ServicesRepository;

class ServicesService
{
    private $repository;

    public function __construct()
    {
        $this->repository = new ServicesRepository();
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function getOne($id)
    {
        return $this->repository->getOne($id);
    }

    public function insert($array)
    {
        //TODO: add validation

        $this->repository->insert($array);
    }

    public function update($array)
    {
        $this->repository->update($array);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }
}