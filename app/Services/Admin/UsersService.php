<?php

namespace App\Services\Admin;


use App\Repositories\UsersRepository;

class UsersService
{
    private $repository;

    public function __construct()
    {
        $this->repository = new UsersRepository();
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function insert($array)
    {
        $array['password'] = bcrypt($array['password']);

        $this->repository->insert($array);
    }
}