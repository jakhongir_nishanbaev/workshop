<?php

namespace App\Services\Admin;


use App\Repositories\RequestsRepository;
use App\Repositories\ServicesRepository;
use App\Repositories\UsersRepository;

class RequestsService
{
    private $repository;
    private $usersRepository;
    private $servicesRepository;

    public function __construct()
    {
        $this->repository = new RequestsRepository();
        $this->usersRepository = new UsersRepository();
        $this->servicesRepository = new ServicesRepository();
    }

    /**
     * Get requests from database.
     */
    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function getUsers()
    {
        return $this->usersRepository->getAll();
    }

    public function getServices()
    {
        return $this->servicesRepository->getAll();
    }

    public function getOne($id)
    {
        return $this->repository->getOne($id);
    }

    public function insert($array)
    {
        //TODO: add validation

        $this->repository->insert($array);
    }

    public function update($array)
    {
        $this->repository->update($array);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }
}