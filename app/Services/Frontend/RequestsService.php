<?php

namespace App\Services\Frontend;


use App\Repositories\RequestsRepository;
use App\Repositories\ServicesRepository;
use App\Traits\AuthUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class RequestsService
{
    use AuthUser;

    private $repository;
    private $servicesRepository;

    public function __construct()
    {
        $this->repository = new RequestsRepository();
        $this->servicesRepository = new ServicesRepository();
    }

    public function getUserRequests() : Collection
    {
        return $this->repository->getAllWhere(['user_id' => $this->getUserId()]);
    }

    public function getServices() : Collection
    {
        return $this->servicesRepository->getAll();
    }

    public function insert($array) : void
    {
        $array = array_merge($array, ['user_id' => $this->getUserId()]);

        $this->repository->insert($array);
    }

    public function getOne($id) : Model
    {
        return $this->repository->getOne($id);
    }
}