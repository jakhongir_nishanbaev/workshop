<?php

namespace App\Services\Api;


use App\Repositories\RequestsRepository;
use App\Repositories\UsersRepository;
use Illuminate\Support\Facades\Hash;

class RequestsService
{
    private $repository;
    private $usersRepository;

    public function __construct()
    {
        $this->repository = new RequestsRepository();
        $this->usersRepository = new UsersRepository();
    }

    public function insert($array)
    {
        try {
            $user = $this->authenticateUser($array['login'], $array['password']);
            $array = array_merge($array, ['user_id' => $user->id]);

            $whiteList = ['user_id', 'visit_date', 'service_id', 'comment'];
            $array = array_intersect_key($array, array_flip($whiteList));

            $this->repository->insert($array);

            return ['result' => 'success', 'error' => 0, 'message' => 'success'];
        } catch (\Exception $exception) {
            return ['result' => new \stdClass(), 'error' => 2, 'message' => $exception->getMessage()];
        }
    }

    public function authenticateUser($login, $password)
    {
        $user = $this->usersRepository->getOneWhere(['login' => $login]);

        if ($user && Hash::check($password, $user->password)) {
            return $user;
        }

        throw new \Exception('Authentication failed.');
    }

    public function getUserRequests($array)
    {
        try {
            $user = $this->authenticateUser($array['login'], $array['password']);

            $requests = $this->repository->onlyColumns(['service_id', 'visit_date', 'comment'])->getAllWhere(['user_id' => $user->id]);

            return ['result' => ['requests' => $requests], 'error' => 0, 'message' => 'success'];
        } catch (\Exception $exception) {
            return ['result' => new \stdClass(), 'error' => 2, 'message' => $exception->getMessage()];
        }
    }
}