<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Interfaces\ControllersContract;
use App\Services\Frontend\RequestsService;
use App\Traits\Serviceable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestsController extends Controller implements ControllersContract
{
    use Serviceable;

    public function __construct()
    {
        $this->setService(new RequestsService());
    }

    public function index()
    {
        $requests = $this->service->getUserRequests();

        return view('frontend.requests.index', [
            'requests' => $requests
        ]);
    }

    public function create()
    {
        $services = $this->service->getServices();

        return view('frontend.requests.create', [
            'services' => $services
        ]);
    }

    public function store(Request $request)
    {
        $this->service->insert($request->except('_token'));

        return redirect()->route('main_page');
    }

    public function single($id)
    {
        $request = $this->service->getOne($id);

        return view('frontend.requests.single', [
            'request' => $request
        ]);
    }
}
