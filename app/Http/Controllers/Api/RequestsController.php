<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Interfaces\ControllersContract;
use App\Services\Api\RequestsService;
use App\Traits\Serviceable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestsController extends Controller implements ControllersContract
{
    use Serviceable;

    public function __construct()
    {
        $this->setService(new RequestsService());
    }

    public function create(Request $request)
    {
        $result = $this->service->insert($request->all());

        return $result;
    }

    public function list(Request $request)
    {
        $result = $this->service->getUserRequests($request->all());

        return $result;
    }
}
