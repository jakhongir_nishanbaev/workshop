<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Interfaces\ControllersContract;
use App\Services\Admin\RequestsService;
use App\Traits\Serviceable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestsController extends Controller implements ControllersContract
{
    use Serviceable;

    public function __construct()
    {
        $this->setService(new RequestsService());
    }

    /**
     * Show requests list page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        $requests = $this->service->getAll();

        return view('admin.requests.list', [
            'items' => $requests
        ]);
    }

    /**
     * Show create request page.
     */
    public function create()
    {
        $users = $this->service->getUsers();
        $services = $this->service->getServices();

        return view('admin.requests.create', [
            'users' => $users,
            'services' => $services
        ]);
    }

    public function edit($id)
    {
        $service = $this->service->getOne($id);
        $users = $this->service->getUsers();
        $services = $this->service->getServices();

        return view('admin.requests.edit', [
            'editItem' => $service,
            'users' => $users,
            'services' => $services
        ]);
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return redirect()->route('admin_list_requests');
    }

    public function store(Request $request)
    {
        $this->service->insert($request->except('_token'));

        return redirect()->route('admin_list_requests');
    }

    public function update(Request $request)
    {
        $this->service->update($request->except('_token'));

        return redirect()->route('admin_list_requests');
    }
}
