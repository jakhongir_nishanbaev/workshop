<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Interfaces\ControllersContract;
use App\Services\Admin\UsersService;
use App\Traits\Serviceable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller implements ControllersContract
{
    use Serviceable;

    public function __construct()
    {
        $this->setService(new UsersService());
    }

    public function list()
    {
        $users = $this->service->getAll();

        return view('admin.users.list', [
            'items' => $users
        ]);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function edit($id)
    {
        $service = $this->service->getOne($id);

        return view('admin.users.edit', [
            'editItem' => $service
        ]);
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return redirect()->route('admin_list_users');
    }

    public function store(Request $request)
    {
        $this->service->insert($request->except('_token'));

        return redirect()->route('admin_list_users');
    }

    public function update(Request $request)
    {
        $this->service->update($request->except('_token'));

        return redirect()->route('admin_list_users');
    }
}
