<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Interfaces\ControllersContract;
use App\Services\Admin\ServicesService;
use App\Traits\Serviceable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesController extends Controller implements ControllersContract
{
    use Serviceable;

    public function __construct()
    {
        $this->setService(new ServicesService());
    }

    public function list()
    {
        $services = $this->service->getAll();

        return view('admin.services.list', [
            'items' => $services
        ]);
    }

    public function create()
    {
        return view('admin.services.create');
    }

    public function edit($id)
    {
        $service = $this->service->getOne($id);

        return view('admin.services.edit', [
            'editItem' => $service
        ]);
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return redirect()->route('admin_list_services');
    }

    public function store(Request $request)
    {
        $this->service->insert($request->except('_token'));

        return redirect()->route('admin_list_services');
    }

    public function update(Request $request)
    {
        $this->service->update($request->except('_token'));

        return redirect()->route('admin_list_services');
    }
}
