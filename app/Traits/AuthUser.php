<?php

namespace App\Traits;


use Illuminate\Support\Facades\Auth;

trait AuthUser
{
    public function getUserId()
    {
        return Auth::user()->id;

//        return 1;
    }
}