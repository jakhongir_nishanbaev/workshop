<?php

namespace App\Traits;


trait Serviceable
{
    private $service;

    public function setService($service)
    {
        $this->service = $service;
    }
}