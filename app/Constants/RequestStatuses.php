<?php

namespace App\Constants;


class RequestStatuses
{
    const PENDING = 1;
    const APPROVED = 2;
    const DECLINED = 0;

    public static function statuses()
    {
        return [
            self::PENDING => 'В ожидании',
            self::APPROVED => 'Принята',
            self::DECLINED => 'Отклонена'
        ];
    }

    public static function getStatusName(int $status) : string
    {
        if (key_exists($status, self::statuses())) {
            return self::statuses()[$status];
        }
    }
}