<?php

namespace App\Repositories\Interfaces;


//use Illuminate\Database\Eloquent\Collection;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface RepositoryContract
{
    public function getOne($id) : Model;

    public function getAll() : Collection;

    public function getOneWhere($array) : Model;

    public function getAllWhere($array) : Collection;

    public function insert($array) : void;

    public function update($array) : void;

    public function delete($id) : void;
}