<?php

namespace App\Repositories\Classes;


class Query
{
    public $queryBuilder;

    private $isPaginated;
    private $perPage;
    private $currentPage;
    private $columns = ['*'];

    public function __construct($queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * Perform query, paginated or all items.
     *
     * @param array $columns
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function execute($columns = ['*'])
    {
        $result = null;

        if ($this->isPaginated) {
            $result = $this->queryBuilder->paginate($this->perPage, $columns, 'page', $this->currentPage);

            $this->removePagination();
        } else {
            $result = $this->queryBuilder->get($this->columns);
        }

        return $result;
    }

    /**
     * Method defines if queried items should be paginated.
     *
     * @param $perPage
     * @param $currentPage
     * @return Query
     */
    public function addPagination($perPage, $currentPage) // TODO: rename to paginate
    {
        $this->isPaginated = true;
        $this->perPage = $perPage;
        $this->currentPage = $currentPage;

        return $this;
    }

    public function setColumns($array) // TODO: rename to selectColumns
    {
        $this->columns = $array;

        return $this;
    }

    /**
     * Turn off pagination for query.
     *
     * @return $this
     */
    private function removePagination()
    {
        $this->isPaginated = false;
        $this->perPage = null;
        $this->currentPage = null;

        return $this;
    }

    /**
     * Delegate all method calls to query builder object.
     *
     * @param $method
     * @param $parameters
     * @return Query|mixed
     */
    public function __call($method, $parameters)
    {
        $result = call_user_func_array([$this->queryBuilder, $method], $parameters);

        return $result === $this->queryBuilder ? $this : $result;
    }
}