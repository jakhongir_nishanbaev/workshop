<?php

namespace App\Repositories;


use App\Repositories\Classes\Query;
use App\Repositories\Interfaces\RepositoryContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

//use Illuminate\Database\Eloquent\Collection;

abstract class Repository implements RepositoryContract
{
    protected $query;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->query = $this->createQuery();
    }

    abstract function createQuery() : Query;

    public function getAll(): Collection
    {
        return $this->query->execute();
    }

    public function getOne($id): Model
    {
        return $this->query->find($id);
    }

    public function getOneWhere($array): Model
    {
        return $this->query->where($array)->first();
    }

    public function getAllWhere($array): Collection
    {
        return $this->query->where($array)->execute();
    }

    public function paginate(int $perPage, int $currentPage) : void
    {
        $this->query->addPagination($perPage, $currentPage);
    }

    public function onlyColumns(array $array) : RequestsRepository
    {
        $this->query->setColumns($array);

        return $this;
    }

    public function insert($array) : void
    {
        $this->query->insert($array);
    }

    public function update($array): void
    {
        $this->query->where('id', $array['id'])->update($array);
    }

    public function delete($id) : void
    {
        $this->query->where('id', $id)->delete();
    }
}