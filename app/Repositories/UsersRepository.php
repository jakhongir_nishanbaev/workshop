<?php

namespace App\Repositories;


use App\Models\User;
use App\Repositories\Classes\Query;
use App\Repositories\Interfaces\RepositoryContract;

class UsersRepository extends Repository implements RepositoryContract
{
    function createQuery(): Query
    {
        return new Query((new User())->query());
    }
}