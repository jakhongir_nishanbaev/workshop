<?php

namespace App\Repositories;


use App\Models\Service;
use App\Repositories\Classes\Query;

class ServicesRepository extends Repository
{
    function createQuery(): Query
    {
        return new Query((new Service())->query());
    }
}