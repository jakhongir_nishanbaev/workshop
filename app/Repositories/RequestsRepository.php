<?php

namespace App\Repositories;


use App\Models\Request;
use App\Repositories\Classes\Query;

class RequestsRepository extends Repository
{
    function createQuery(): Query
    {
        return new Query((new Request())->query());
    }
}